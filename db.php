<?php
class Database
{
    private static $dbName = 'books' ;
    private static $dbHost = 'localhost' ;
    private static $dbUsername = 'mysql';
    private static $dbUserPassword = 'mysql';
     
    private static $cont  = null;
     
    public function __construct() {
        die('Нет конструктора');
    }
     
    public static function connect()
    {
       if ( null == self::$cont )
       {     
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword); 
        }
        catch(PDOException $e)
        {
          die($e->getMessage()); 
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
}
?>