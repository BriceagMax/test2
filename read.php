<?php
    require 'db.php';
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: index.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Books where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
 
<body>
    <div class="container">
          <div class="row">
                <h3>Информация о книге</h3>
            </div>
        <div class="row">
            
             <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Ид</th>
                        <th>Название</th>
                        <th>Автор</th>
                        <th>Год выпуска</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $data['id'];?></td>
                        <td><?php echo $data['name'];?></td>
                        <td><?php echo $data['autor'];?></td>
                        <td><?php echo $data['year'];?></td>
                    </tr>
                 </tbody>
            </table>
            <div class="form-actions">
                <a class="btn btn-danger" href="index.php">Вернуться</a>
            </div>   
        </div>   
    </div>
</body>
</html>