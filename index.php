<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
 
<body>
    <div class="container">
            <div class="row">
                <h3>Книги_База_Данных</h3>
            </div>
            <div class="row">
			          <p>
                    <a href="new.php" class="btn btn-info">Создать</a>
                    <a href="home.php" class="btn btn-danger">Вернуться</a>  
                </p>
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Ид</th>
                      <th>Название</th>
                      <th>Автор</th>
					            <th>Год выпуска</th>
					            <th>Действия</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                   include 'db.php';
                   $pdo = Database::connect();
                   $sql = 'SELECT * FROM Books ORDER BY id ASC';
                   foreach ($pdo->query($sql) as $row) {
                            echo '<tr>';
                            echo '<td>'. $row['id'] . '</td>';
                            echo '<td>'. $row['name'] . '</td>';
                            echo '<td>'. $row['autor'] . '</td>';
							echo '<td>'. $row['year'] . '</td>';
							echo '<td width=300>';
                            echo '<a class="btn btn-primary" href="read.php?id='.$row['id'].'">Данные</a>';
                            echo ' ';
                            echo '<a class="btn btn-success" href="update.php?id='.$row['id'].'">Изменить</a>';
                            echo ' ';
                            echo '<a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Удалить</a>';
                            echo '</td>';
                            echo '</tr>';
                   }
                   Database::disconnect();
                  ?>
                  </tbody>
            </table>
        </div>
    </div> 
  </body>
</html>