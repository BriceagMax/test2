<?php
    require 'db.php';
 
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: index.php");
    }
     
    if ( !empty($_POST)) {
        // объявляем переменные ошибки ввода
        $nameError = null;
        $emailError = null;
        $mobileError = null;
         
        // сохраняем значения из html формы
        $name = $_POST['name'];
        $email = $_POST['autor'];
        $mobile = $_POST['year'];
         
        // проверки ввода
        $valid = true;
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }
         
        if (empty($autor)) {
            $autorError = 'Please enter Autor';
            $valid = false;
        } 
		       
        if (empty($year)) {
            $yearError = 'Please enter Year';
            $valid = false;
        }
         
        // изменение данных
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE Books  set name = ?, autor = ?, year =? WHERE id = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($name,$autor,$year,$id));
            Database::disconnect();
            header("Location: index.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Books where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $name = $data['name'];
        $autor = $data['autor'];
        $year = $data['year'];
        Database::disconnect();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Изменить книгу</h3>
                    </div>
             
                    <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">
					<div class="control-group <?php echo !empty($idError)?'error':'';?>">
                        <label class="control-label">Ид</label>
                        <div class="controls">
                            <input name="id" type="text"  placeholder="Ид" value="<?php echo !empty($id)?$id:'';?>">
                            <?php if (!empty($idError)): ?>
                                <span class="help-inline"><?php echo $idError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">Название</label>
                        <div class="controls">
                            <input name="name" type="text"  placeholder="Название" value="<?php echo !empty($name)?$name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <span class="help-inline"><?php echo $nameError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($autorError)?'error':'';?>">
                        <label class="control-label">Автор</label>
                        <div class="controls">
                            <input name="autor" type="text" placeholder="Автор" value="<?php echo !empty($autor)?$autor:'';?>">
                            <?php if (!empty($autorError)): ?>
                                <span class="help-inline"><?php echo $autorError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($yearError)?'error':'';?>">
                        <label class="control-label">Год выпуска</label>
                        <div class="controls">
                            <input name="year" type="text"  placeholder="Год выпуска" value="<?php echo !empty($year)?$year:'';?>">
                            <?php if (!empty($yearError)): ?>
                                <span class="help-inline"><?php echo $yearError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Изменить</button>
                          <a class="btn btn-danger" href="index.php">Вернуться</a>
                        </div>
                    </form>
                </div>
                 
    </div> 
  </body>
</html>