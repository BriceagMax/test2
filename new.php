<?php
     
    require 'db.php';
 
    if ( !empty($_POST)) {
        //объявляем переменные ошибки ввода
        $idError = null;
        $nameError = null;
        $autorError = null;
		$yearError = null;
         
        // сохраняем значения из html формы
		$id = $_POST['id'];
        $name = $_POST['name'];
        $autor = $_POST['autor'];
        $year = $_POST['year'];
         
        // проверка ввода
        $valid = true;
		 if (empty($id)) {
            $idError = 'Please enter Id';
            $id = false;
        }
		
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }
         
        if (empty($autor)) {
            $autorError = 'Please enter Autor';
            $valid = false;
        } 
		
        if (empty($year)) {
            $yearError = 'Please enter Year';
            $valid = false;
        }
         
        // Записываем новые данные
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO Books (id,name,autor,year) values(?,?, ?, ?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($id,$name,$autor,$year));
            Database::disconnect();
            header("Location: index.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Добавить книгу</h3>
                    </div>
             
                    <form class="form-horizontal" action="new.php" method="post">
					 <div class="control-group <?php echo !empty($idError)?'error':'';?>">
                        <label class="control-label">Ид</label>
                        <div class="controls">
                            <input name="id" type="text"  placeholder="Ид" value="<?php echo !empty($id)?$id:'';?>">
                            <?php if (!empty($idError)): ?>
                                <span class="help-inline"><?php echo $idError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">Название</label>
                        <div class="controls">
                            <input name="name" type="text"  placeholder="Название" value="<?php echo !empty($name)?$name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <span class="help-inline"><?php echo $nameError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($autorError)?'error':'';?>">
                        <label class="control-label">Автор</label>
                        <div class="controls">
                            <input name="autor" type="text" placeholder="Автор" value="<?php echo !empty($autor)?$autor:'';?>">
                            <?php if (!empty($autorError)): ?>
                                <span class="help-inline"><?php echo $autorError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($yearError)?'error':'';?>">
                        <label class="control-label">Год выпуска</label>
                        <div class="controls">
                            <input name="year" type="text"  placeholder="Год выпуска" value="<?php echo !empty($year)?$year:'';?>">
                            <?php if (!empty($yearError)): ?>
                                <span class="help-inline"><?php echo $yearError;?></span>
                            <?php endif;?>
                        </div>
                      </div>
					  <br>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Добавить</button>
                          <a class="btn btn-danger" href="index.php">Вернуться</a>
                        </div>
                    </form>
                </div>
                 
    </div>
  </body>
</html>